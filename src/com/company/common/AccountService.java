package com.company.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by aleksandr on 10-Oct-16.
 */
public interface AccountService extends Remote {
    /**
     * Retrieves current balance or zero if addAmount() method was not called before
     * @param id
     * @return
     * @throws RemoteException
     */
    public Long getAmount(Integer id) throws RemoteException;

    /**
     * Increases balance or set if addAmount() method was called first time
     * @param id
     * @param value
     * @throws RemoteException
     */
    public void addAmount(Integer id, Long value) throws RemoteException;
}
