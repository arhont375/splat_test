package com.company.server.database;

/**
 * Created by aleksandr on 12-Oct-16.
 */
class DatabaseContract {
    private DatabaseContract() {}

    static class AccountsContract{
        private AccountsContract() {}

        public static final String TABLE_NAME = "accounts";
        public static final String KEY_ID = "id";
        public static final String KEY_AMOUNT = "amount";

        public static final String CREATE_TABLE_IFNE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
                + "( " + KEY_ID + " INT PRIMARY KEY NOT NULL,"
                +       KEY_AMOUNT + " INT NOT NULL)";

        public static final String UPSERT = "INSERT INTO accounts (id, amount) VALUES (?, ?) " +
                "ON CONFLICT (id) DO UPDATE SET amount = ?";
        public static final String SELECT = "SELECT id, amount FROM accounts WHERE id = ?";
    }
}
