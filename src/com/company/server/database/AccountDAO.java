package com.company.server.database;

import com.company.common.AccountService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by aleksandr on 12-Oct-16.
 */
public final class AccountDAO {
    private AccountDAO(){
    }

    public static void UpdateOrInsert(Integer accountId, Long amount, Connection connection) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DatabaseContract.AccountsContract.UPSERT)) {
            preparedStatement.setInt(1, accountId);
            preparedStatement.setLong(2, amount);
            preparedStatement.setLong(3, amount);

            preparedStatement.executeUpdate();
        }
    }

    public static Long Read(Integer accountId, Connection connection) throws SQLException {
        ResultSet resultSet = null;
        Long amount = 0L;
        try (PreparedStatement preparedStatement = connection.prepareStatement(DatabaseContract.AccountsContract.SELECT)) {
            preparedStatement.setInt(1, accountId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                amount = resultSet.getLong("amount");
            }
        }

        return amount;
    }
}
