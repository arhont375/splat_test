package com.company.server.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by aleksandr on 12-Oct-16.
 */
public class DbManager {
    private Connection connection;
    private static DbManager Instance = null;

    public static DbManager getInstance() {
        if (Instance == null) {
            Instance = new DbManager();
        }
        return Instance;
    }

    private DbManager(){
        connect();
    }

    public Connection getConnection() {
        return connection;
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }

    private void connect(){
        connection = null;
        Statement statement = null;
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",  "postgres", "test");
            System.out.println("DB opened successfully");
            createIfNotExist();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void createIfNotExist() throws SQLException {
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(DatabaseContract.AccountsContract.CREATE_TABLE_IFNE);
        }
    }
}
