package com.company.server;

import com.company.common.AccountService;
import com.company.server.cache.BicycleCache;
import com.company.server.database.AccountDAO;
import com.company.server.database.DbManager;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by aleksandr on 12-Oct-16.
 */
public class Server extends UnicastRemoteObject implements AccountService {
    private static AtomicInteger totalQueries = new AtomicInteger(0);
    private static AtomicInteger currentQueries = new AtomicInteger(0);
    private static BicycleCache<Integer, Long> bicycleCache = new BicycleCache<>();

    public Server() throws RemoteException {
        super(0);
    }

    @Override
    public Long getAmount(Integer id) throws RemoteException {
        currentQueries.incrementAndGet();
        totalQueries.incrementAndGet();

        if (id == null) {
            throw new IllegalArgumentException("Something wrong with your parameters");
        }

        Long amount = bicycleCache.get(id);
        if (amount == null) {
            try {
                amount = AccountDAO.Read(id, DbManager.getInstance().getConnection());
                bicycleCache.put(id, amount);
            } catch (SQLException e) {
                throw new RemoteException("Error when read from DB", e);
            }
        }

        currentQueries.decrementAndGet();
        return amount;
    }

    @Override
    public void addAmount(Integer id, Long value) throws RemoteException {
        currentQueries.incrementAndGet();
        totalQueries.incrementAndGet();

        if (value == null || id == null) {
            throw new IllegalArgumentException("Something wrong with your parameters");
        }

        try {
            bicycleCache.remove(id);
            AccountDAO.UpdateOrInsert(id, value, DbManager.getInstance().getConnection());
        } catch (SQLException e) {
            throw new RemoteException("Error when upsert in DB", e);
        }

        currentQueries.decrementAndGet();
    }

    public static void resetStatistic() {
        totalQueries.set(0);
    }

    public static Integer getTotalQueries() {
        return totalQueries.get();
    }

    public static Integer getCurrentQueries() {
        return currentQueries.get();
    }
}
