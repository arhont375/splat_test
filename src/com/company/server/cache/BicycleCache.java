package com.company.server.cache;

import java.util.concurrent.*;

/**
 * Created by aleksandr on 12-Oct-16.
 */
public class BicycleCache<TKey, TValue> {
    private ConcurrentHashMap<TKey, CacheValue<TValue>> concurrentMap;
    private int timeToClean = 2500; //in ms
    private int ttl = 1000; //in ms

    public BicycleCache() {
        concurrentMap = new ConcurrentHashMap<>();
        runCleaner();
    }

    /**
     * Get from cache
     * @param key
     * @return
     */
    public synchronized TValue get(TKey key) {
        CacheValue<TValue> value = concurrentMap.get(key);
        return value == null ? null : value.value;
    }

    /**
     * Insert or update in cache
     * @param key
     * @param value
     */
    public synchronized void put(TKey key, TValue value) {
        concurrentMap.put(key, new CacheValue<TValue>(System.currentTimeMillis(), value));
    }

    /**
     * Remove from cache
     * @param key
     */
    public synchronized void remove(TKey key) {
        concurrentMap.remove(key);
    }

    private void setTimeToClean(int timeToClean) {
        this.timeToClean = timeToClean;
    }

    private void setTtl(int ttl) {
        this.ttl = ttl;
    }

    private void runCleaner(){
        scheduledExecutorService.scheduleAtFixedRate(new CacheCleaner(), 0, timeToClean, TimeUnit.MILLISECONDS);
        System.out.println("Cleaner started");
    }

    private ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        }
    });

    private class CacheCleaner implements Runnable {
        @Override
        public void run() {
            Long currentTime = System.currentTimeMillis() - ttl;
            concurrentMap.entrySet().stream().filter(record -> currentTime > record.getValue().lastAccessed).forEach(record -> {
                concurrentMap.remove(record.getKey());
            });
        }
    }
}
