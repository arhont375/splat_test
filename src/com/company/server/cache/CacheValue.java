package com.company.server.cache;

/**
 * Created by aleksandr on 12-Oct-16.
 */
class CacheValue<T> {
    public Long lastAccessed;
    public T value;

    public CacheValue(Long lastAccessed, T value) {
        this.lastAccessed = lastAccessed;
        this.value = value;
    }
}
