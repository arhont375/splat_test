package com.company.server;

import com.company.server.database.DbManager;
import com.company.server.util.LogService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class Main  {
    public static void main(String args[]) throws Exception {
        System.out.println("RMI server started");

        try { //special exception handler for registry creation
            LocateRegistry.createRegistry(1099);
            System.out.println("java RMI registry created.");
        } catch (RemoteException e) {
            //do nothing, error means registry already exists
            System.out.println("java RMI registry already exists.");
        }

//        System.setProperty("java.security.policy", "./permission.policy");
        // Security manager
//        if (System.getSecurityManager() == null) {
//            System.setSecurityManager(new SecurityManager());
//        }


        // Init singletons
        DbManager.getInstance();

        //Instantiate Server
        Server server = new Server();

        //Run logging
        LogService.runLogging();

        // Bind this object instance to the name "Server"
        Naming.rebind("//localhost/Server", server);
        System.out.println("PeerServer bound in registry");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                String s = bufferedReader.readLine();

                if (s.isEmpty()) {
                    continue;
                }

                s = s.toLowerCase();

                switch (s.toLowerCase()) {
                    case "reset" : Server.resetStatistic();
                        break;
                    case "stat" : System.out.println("Total processed: " + Server.getTotalQueries() + "  Currently on processing: " + Server.getCurrentQueries());
                        break;
                    default:
                        System.exit(0);
                }

                if (s.contains("reset")) {
                    Server.resetStatistic();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}