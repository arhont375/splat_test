package com.company.server.util;

import com.company.server.Server;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

/**
 * Created by aleksandr on 12-Oct-16.
 */
public class LogService implements Runnable {
    private static final int TIME_TO_LOG = 1000; //in ms
    private static final Logger logger = Logger.getLogger(LogService.class.getName());
    private static final ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        }
    });

    public static void runLogging(){
        try {
            logger.addHandler(new FileHandler("splat_test_server.log", true));
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.setUseParentHandlers(false);
        scheduledExecutorService.scheduleAtFixedRate(new LogService(), 0, TIME_TO_LOG, TimeUnit.MILLISECONDS);
        System.out.println("Log initialized");
    }

    @Override
    public void run() {
        logger.info("Total processed: " + Server.getTotalQueries() + "  Currently on processing: " + Server.getCurrentQueries());
    }
}
