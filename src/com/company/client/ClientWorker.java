package com.company.client;

import com.company.common.AccountService;

import java.rmi.RemoteException;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by aleksandr on 12-Oct-16.
 */
public class ClientWorker implements Runnable {
    private static final int ITERATIONS = 1000;
    private static final int TIME_TO_SLEEP = 50;
    private static final long MAX_ADD_AMOUNT = 1000;

    private Configuration configuration;
    private AccountService accountService;
    private Boolean isReader;

    public ClientWorker(Configuration configuration, AccountService accountService, Boolean isReader) {
        this.configuration = configuration;
        this.accountService = accountService;
        this.isReader = isReader;
    }

    @Override
    public void run() {
        int cnt = 0;
        while (cnt < ITERATIONS) {
            try {
                if (isReader) {
                    System.out.println(Thread.currentThread().getName() + " "  + accountService.getAmount(getRandomId()));
                } else {
                    accountService.addAmount(getRandomId(), ThreadLocalRandom.current().nextLong() % MAX_ADD_AMOUNT);
                }
            } catch (RemoteException re) {
                System.out.println(re.getMessage());
            }

            System.out.println("In thread: " + Thread.currentThread().getName());

            try{
                Thread.sleep(TIME_TO_SLEEP);
            } catch (InterruptedException ie)
            {}
            ++cnt;
        }
    }

    private Integer getRandomId() {
        return configuration.getIds().get(ThreadLocalRandom.current().nextInt(configuration.getIds().size()));
    }
}
