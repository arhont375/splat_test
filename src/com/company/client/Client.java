package com.company.client;

import com.company.common.AccountService;

import java.rmi.Naming;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by aleksandr on 10-Oct-16.
 */
public class Client {
    public static void main(String args[]) throws Exception {

        if (args.length == 0 || args.length < 4) {
            System.out.println("For work with this app you must specify ServerIP, rCount, wCount and idList");
            System.out.println("ServerIP example: 127.0.0.1");
            System.out.println("rCount example: 5");
            System.out.println("wCount example: 4");
            System.out.println("idList example: 12-17 or 12,13,14,15,16");
            System.out.println("all args: 127.0.0.1 5 4 12-17");
            return;
        }
        Configuration configuration = ParseArguments(args);

        AccountService accountService = (AccountService) Naming.lookup("//" + configuration.getIpAddress() + "/Server");

        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for (int i = 0, rCount = configuration.getrCount(); i < rCount; ++i) {
            executorService.execute(new ClientWorker(configuration, accountService, true));
        }
        for (int i = 0, wCount = configuration.getwCount(); i < wCount; ++i) {
            executorService.execute(new ClientWorker(configuration, accountService, false));
        }

        executorService.shutdown();
        while (!executorService.isTerminated()) {
        }

        System.out.println("All threads finished");
    }

    private static Configuration ParseArguments(String args[]) {
        List<Integer> ids;
        if (args[3].contains("-")) {
            String[] tokens = args[3].split("-");
            ids = IntStream.rangeClosed(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1])).boxed().collect(Collectors.toList());
        } else {
            String[] tokens = args[3].split(",");
            ids = Arrays.stream(tokens).map(s -> Integer.parseInt(s)).collect(Collectors.toList());
        }

        return new Configuration(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]), ids);
    }
}
