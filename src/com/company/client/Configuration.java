package com.company.client;

import java.util.List;

/**
 * Created by aleksandr on 10-Oct-16.
 */
public class Configuration {
    private String ipAddress;
    private Integer rCount;
    private Integer wCount;
    private List<Integer> ids;
    
    public Configuration(String ipAddress, Integer rCount, Integer wCount, List<Integer> ids) {
        this.ipAddress = ipAddress;
        this.rCount = rCount;
        this.wCount = wCount;
        this.ids = ids;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public Integer getrCount() {
        return rCount;
    }

    public Integer getwCount() {
        return wCount;
    }

    public List<Integer> getIds() {
        return ids;
    }
}
